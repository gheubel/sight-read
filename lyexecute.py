import platform
import subprocess

# from mingus.midi import fluidsynth
# fluidsynth.init("./soundfonts/PIANO.sf2")

def lyformat(notes):
    """
    Takes a list of notes formatted by Mingus and exports a string to be written to Lilypond.
    """

    lyform = ' '.join(x for x in notes if x not in ',\'[]()')
    lyform = ''.join("f" if char == "b" else char for char in lyform)
    lyform = ''.join("s" if char == "#" else char for char in lyform)
    return lyform.lower()

def lywrite(notes,relative="c''"):
    """
    Writes a string of notes formatted for Lilypond to score.ly.
    """

    f = open("score.ly","w")
    f.write(
"""
\\version \"2.20.0\"
\\language "english"
\\relative {0}

{{
    #(set-global-staff-size 32)
    \\clef "treble"
    \\time 4/4
    {1}
}}
"""
    .format(relative, notes))
    f.close()

def lycompile():
    """
    Runs Lilypond on score.ly for the appropriate system.
    """

    if (platform.system() == "Windows"):
        subprocess.run(['wsl', 'lilypond','score.ly'])

    else:
        subprocess.run(['lilypond','score.ly'])