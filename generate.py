import random
from mingus.containers import Note
import mingus.core.notes as notes
import mingus.core.scales as scales
import mingus.core.keys as keys
import mingus.core.intervals as intervals

def genRandom(note_count=16,beat_choice="quarter"):
    """
    Generates a random list of Mingus-formatted notes. Int parameter "note_count" determines how many notes are generated in the list, 
    and string parameter "beat_choice" determines the length of each note.
    """

    NOTELIST_SHARP = scales.Chromatic('C').ascending()
    NOTELIST_FLAT = [x for x in reversed(scales.Chromatic('C').descending())]
    NOTELIST_TRUE = random.choice([NOTELIST_FLAT,NOTELIST_SHARP])

    beat = {"whole":"1","half":"2","quarter":"4","8th":"8","16th":"16","32nd":"32"}

    random_notes = random.choices(NOTELIST_TRUE,k=note_count)
    random_notes = [x + beat[beat_choice] for x in random_notes]

    return random_notes

def genMelody(key="C"):
    """
    Generates a coherent melody of Mingus-formatted notes.
    """
    pass