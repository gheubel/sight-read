### For Windows systems, Sightread requires Windows Subsystems for Linux (WSL) to access Lilypond. Install WSL via Windows Powershell via 
    wsl --install
### Sightread is highly dependent upon GNU Lilypond 2.20.0 to format and interpret text into music notation. Learn more at http://lilypond.org/index.html. Install Lilypond with WSL via 
    sudo apt install lilypond
### Mac systems with Homebrew can install Lilypond via
    brew install lilypond

![](https://c.tenor.com/411cWj7IonQAAAAC/the-rock-rock.gif)
